package sergey;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import redis.embedded.RedisServer;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;

@Configuration
public class EmbeddedRedisTestConfiguration {

  private RedisServer server;

  public EmbeddedRedisTestConfiguration(@Value("${spring.redis.port}") Integer redisPort) throws IOException {
    server = RedisServer
        .builder()
        .setting("maxmemory 128M")
        .port(redisPort)
        .build();
  }

  @PostConstruct
  public void startRedis() {
    server.start();
  }

  @PreDestroy
  public void stopRedis() {
    server.stop();
  }
}
