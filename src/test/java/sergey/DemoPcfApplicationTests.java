package sergey;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import sergey.service.StorageService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoPcfApplicationTests {

	@Autowired
	private StorageService storageService;

	@Test
	public void contextLoads() {
	}


	@Test
	public void testStoring() throws Exception {
		storageService.put("hello", "ehlo");
		Assert.assertEquals(storageService.get("hello"), "ehlo");
	}
}
