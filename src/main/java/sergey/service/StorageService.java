package sergey.service;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class StorageService {
  @Value("${app.redis.storage_name}")
  private String storageName;
  @Autowired
  private RedisTemplate<String, String> redisTemplate;

  public void put(String key, String value){
    log.info("Putting a value into the redis, key {}", key);
    redisTemplate.boundHashOps(storageName).put(key, value);
  }

  public String get(String key){
    log.info("Getting a value with the key {}", key);
    return String.valueOf(redisTemplate.boundHashOps(storageName).get(key));
  }
}
