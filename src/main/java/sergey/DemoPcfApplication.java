package sergey;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoPcfApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoPcfApplication.class, args);
	}
}
