package sergey;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import sergey.service.StorageService;

@Controller
public class SimpleController {
  @Autowired
  private StorageService storageService;

  @RequestMapping("/put/{key}/{value}")
  @ResponseBody
  public String put(
      @PathVariable("key") String key,
      @PathVariable("value") String value) {
    storageService.put(key, value);
    return "ok";
  }

  @RequestMapping("/get/{key}")
  @ResponseBody
  public String get(@PathVariable("key") String key) {
    return storageService.get(key);
  }

  @RequestMapping("/hello")
  @ResponseBody
  public String hello(){
    return "hello";
  }
}
